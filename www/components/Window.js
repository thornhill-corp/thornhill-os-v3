import { colors, validate as validateColor } from '../helpers/colors.js';

const
    SIZE_SMALL = 14,
    SIZE_NORMAL = 19,
    SIZE_BIG = 22,
    BORDER_WIDTH = 3,
    TITLE_HEIGHT = 30,
    SUBTITLE_HEIGHT = 25,
    TEXT_PADDING = 8,
    PROGRESS_PADDING = 3;

/** @class {Konva.Layer} */
let _layer;

/**
 * @class Window
 * @param Konva
 * @param {Number} positionX
 * @param {Number} positionY
 * @param {Number} width
 * @param {String} titleText
 * @param {String} [titleColor=white]
 * @param {Boolean} [isTitleLoading=false]
 * @param {Boolean} [isTitleWarning=false]
 * @param {String} [subtitleText]
 * @param {String} [subtitleMetaText]
 * @param {Boolean} [isSubtitleLoading=false]
 * @param {Boolean} [isSubtitleError=false]
 * @param {Number} [progress]
 */
function Window({
    positionX,
    positionY,
    width,
    titleText,
    titleColor = 'white',
    isTitleLoading = false,
    isTitleWarning = false,
    subtitleText,
    subtitleMetaText,
    isSubtitleLoading = false,
    isSubtitleError = false,
    progress
}){
    const
        _ = {},
        $ = {
            group: new Konva.Group(),
            rectangle: new Konva.Rect({
                stroke: colors.white,
                strokeWidth: BORDER_WIDTH,
                cornerRadius: 6
            }),
            titleBackground: new Konva.Rect({
                fill: colors.black,
                height: TITLE_HEIGHT
            }),
            titleWarning: new Konva.Text({
                height: TITLE_HEIGHT,
                verticalAlign: 'middle',
                fill: colors.white,
                fontSize: 16,
                fontFamily: 'Futura',
                letterSpacing: 2,
                text: '[!]'
            }),
            titleWarningBlink: new Konva.Animation(({ time }) => {
                const
                    T = 1000,
                    d = time % T;
                $.titleWarning.opacity(
                    d < T / 2
                    ? d / (T / 2)
                    : 1 - d / T
                );
            }),
            titleText: new Konva.Text({
                height: TITLE_HEIGHT,
                verticalAlign: 'middle',
                fontSize: SIZE_BIG,
                fontFamily: 'Futura',
                letterSpacing: 1.25
            }),
            titleLoading: new Konva.Animation(({ time }) => {
                const d = time % 2000;
                if(d < 500) $.titleText.text(_.titleText.toUpperCase());
                else if(d < 1000) $.titleText.text(`${_.titleText.toUpperCase()}.`);
                else if(d < 1500) $.titleText.text(`${_.titleText.toUpperCase()}..`);
                else $.titleText.text(`${_.titleText.toUpperCase()}...`);
            }),
            subtitleBackground: new Konva.Rect({
                fill: colors.white,
                height: SUBTITLE_HEIGHT
            }),
            subtitleText: new Konva.Text({
                fill: colors.black,
                height: SUBTITLE_HEIGHT,
                verticalAlign: 'middle',
                fontSize: SIZE_NORMAL,
                fontFamily: 'Futura'
            }),
            subtitleMetaText: new Konva.Text({
                fill: colors.black,
                opacity: 0.75,
                height: SUBTITLE_HEIGHT,
                verticalAlign: 'middle',
                align: 'right',
                fontSize: SIZE_SMALL,
                fontFamily: 'Futura'
            }),
            subtitleLoading: new Konva.Animation(({ time }) => {
                const d = time % 2000;
                if(d < 500) $.subtitleText.text(_.subtitleText.toUpperCase());
                else if(d < 1000) $.subtitleText.text(`${_.subtitleText.toUpperCase()}.`);
                else if(d < 1500) $.subtitleText.text(`${_.subtitleText.toUpperCase()}..`);
                else $.subtitleText.text(`${_.subtitleText.toUpperCase()}...`);
            }),
            progressBar: new Konva.Rect({
                fill: colors.darkgray,
                height: SIZE_NORMAL
            }),
            progressText: new Konva.Text({
                fill: colors.white,
                fontSize: SIZE_NORMAL - PROGRESS_PADDING * 2,
                align: 'right'
            })
        };

    const _updateHeight = () => {
        let height = 0;
        if(this.getTitleText())
            height += TITLE_HEIGHT;
        if(this.getSubtitleText() || this.getProgress())
            height += SUBTITLE_HEIGHT;
        $.rectangle.height(height);
    };

    let _isInit = true;

    /**
     * @method isDestroyed
     * @return {Boolean}
     */
    this.isDestroyed = () => !_isInit && !Window.windows.includes(this);

    /**
     * @method isShown
     * @return {Boolean}
     */
    this.isShown = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return !!$.group.getLayer();
    };

    /**
     * @method show
     */
    this.show = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(this.isShown()) throw new Error('Expected window hidden from view');
        _layer.add($.group);
    };

    /**
     * @method hide
     */
    this.hide = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(!this.isShown()) throw new Error('Expected window shown in view');
        return $.group.remove();
    };

    /**
     * @method getPositionX
     * @return {Number}
     */
    this.getPositionX = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return $.group.x();
    };

    /**
     * @method setPositionX
     * @param {Number} positionX
     */
    this.setPositionX = positionX => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(typeof positionX !== 'number') throw new TypeError('positionX: Expected number');
        if(positionX < 0) throw new RangeError('positionX: Expected positive number');
        if(positionX === this.getPositionX() && !_isInit) throw new Error('positionX: Expected different from current');
        $.group.x(positionX);
        if(_isInit){
            $.rectangle.x(positionX);
            $.titleBackground.x(positionX);
            $.titleWarning.x(positionX + TEXT_PADDING);
            $.titleText.x(positionX + TEXT_PADDING);
            $.subtitleBackground.x(positionX);
            $.subtitleText.x(positionX + BORDER_WIDTH);
            $.subtitleMetaText.x(positionX + BORDER_WIDTH);
            $.progressBar.x(positionX + BORDER_WIDTH);
            $.progressText.x(positionX + BORDER_WIDTH);
        }
    };

    /**
     * @method getPositionY
     * @return {Number}
     */
    this.getPositionY = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return $.group.y();
    };

    /**
     * @method setPositionY
     * @param {Number} positionY
     */
    this.setPositionY = positionY => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(typeof positionY !== 'number') throw new TypeError('positionY: Expected number');
        if(positionY < 0) throw new RangeError('positionY: Expected positive number');
        if(positionY === this.getPositionY() && !_isInit) throw new Error('positionY: Expected different from current');
        $.group.y(positionY);
        if(_isInit){
            $.rectangle.y(positionY);
            $.titleBackground.y(positionY);
            $.titleWarning.y(positionY);
            $.titleText.y(positionY);
            $.subtitleBackground.y(positionY + TITLE_HEIGHT);
            $.subtitleText.y(positionY + TITLE_HEIGHT);
            $.subtitleMetaText.y(positionY + TITLE_HEIGHT);
            $.progressBar.y(positionY + TITLE_HEIGHT + PROGRESS_PADDING);
            $.progressText.y(positionY + TITLE_HEIGHT + PROGRESS_PADDING * 2);
        }
    };

    /**
     * @method getWidth
     * @return {Number}
     */
    this.getWidth = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return $.group.width();
    };

    /**
     * @method setWidth
     * @param {Number} width
     */
    this.setWidth = width => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(typeof width !== 'number') throw new TypeError('width: Expected number');
        if(width === this.getWidth()) throw new Error('width: Expected different from current');
        $.group.width(width);
        $.rectangle.width(width);
        $.titleBackground.width(width);
        $.titleText.width(width - TEXT_PADDING);
        $.subtitleBackground.width(width);
        $.subtitleText.width(width - BORDER_WIDTH);
        $.subtitleMetaText.width(width - BORDER_WIDTH);
    };

    /**
     * @method getTitleText
     * @return {String}
     */
    this.getTitleText = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return _.titleText;
    };

    /**
     * @method setTitleText
     * @param {String} titleText
     */
    this.setTitleText = titleText => {
        if(typeof titleText !== 'string') throw new TypeError('titleText: Expected string');
        if(titleText.length === 0) throw new TypeError('titleText: Expected non-empty string');
        if(titleText === this.getTitleText()) throw new Error('titleText: Expected different from current');
        if(_isInit){
            $.group.add($.titleBackground);
            $.group.add($.titleText);
        }
        $.titleText.text(titleText.toUpperCase());
        _.titleText = titleText;
        _updateHeight();
    };

    /**
     * @method getTitleColor
     * @return {String}
     */
    this.getTitleColor = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return Object.fromEntries(Object.entries(colors).map(_ => _.reverse()))[$.titleText.fill()];
    };

    /**
     * @method setTitleColor
     * @param {String} titleColor
     */
    this.setTitleColor = titleColor => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(typeof titleColor !== 'string') throw new TypeError('titleColor: Expected string');
        if(!validateColor(titleColor)) throw new TypeError('titleColor: Expected valid color');
        if(titleColor === this.getTitleColor()) throw new Error('titleColor: Expected different from current');
        $.titleText.fill(colors[titleColor]);
    };

    /**
     * @method isTitleLoading
     * @return {Boolean}
     */
    this.isTitleLoading = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return $.titleLoading.isRunning();
    };

    /**
     * @method
     * @param {Boolean} isTitleLoading
     */
    this.setTitleLoading = isTitleLoading => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(typeof isTitleLoading !== 'boolean') throw new TypeError('isTitleLoading: Expected boolean');
        if(isTitleLoading){
            if(this.isTitleLoading()) throw new Error('Expected title not loading');
            $.titleLoading.start();
        }
        else {
            if(!this.isTitleLoading()) throw new Error('Expected title loading');
            $.titleLoading.stop();
            $.titleText.text(_.titleText.toUpperCase());
        }
    }

    /**
     * @method isTitleWarning
     * @return {Boolean}
     */
    this.isTitleWarning = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return !![...$.titleWarning.getAncestors()].length;
    };

    /**
     * @method setTitleWarning
     * @param {Boolean} isTitleWarning
     */
    this.setTitleWarning = isTitleWarning => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(typeof isTitleWarning !== 'boolean') throw new TypeError('isTitleWarning: Expected boolean');
        const
            titleTextCurrentPadding = $.titleText.x(),
            titleTextWarningPadding = $.titleWarning.measureSize($.titleWarning.text()).width + TEXT_PADDING;
        if(isTitleWarning){
            if(this.isTitleWarning()) throw new Error('Expected title not warning');
            $.group.add($.titleWarning);
            $.titleText.x(titleTextCurrentPadding + titleTextWarningPadding);
            $.titleWarningBlink.start();
        }
        else {
            if(!this.isTitleWarning()) throw new Error('Expected title warning');
            $.titleWarning.remove();
            $.titleText.x(titleTextCurrentPadding - titleTextWarningPadding);
            $.titleWarningBlink.stop();
        }
    };

    /**
     * @method getSubtitleText
     * @return {String}
     */
    this.getSubtitleText = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return _.subtitleText;
    };

    /**
     * @method setSubtitleText
     * @param {String} subtitleText
     */
    this.setSubtitleText = subtitleText => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(typeof subtitleText !== 'string') throw new TypeError('subtitleText: Expected string');
        if(subtitleText.length === 0) throw new TypeError('subtitleText: Expected non-empty string');
        if(subtitleText === this.getSubtitleText()) throw new Error('subtitleText: Expected different from current');
        $.group.add($.subtitleBackground);
        $.group.add($.subtitleText);
        $.subtitleText.text(subtitleText.toUpperCase());
        _.subtitleText = subtitleText;
        _updateHeight();
    };

    /**
     * @method unsetSubtitleText
     */
    this.unsetSubtitleText = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(!this.getSubtitleText()) throw new Error('Expected subtitle text set');
        if(this.getSubtitleMetaText()) throw new Error('Expected subtitle meta text unset');
        $.subtitleBackground.remove();
        $.subtitleText.remove();
        _.subtitleText = undefined;
        _updateHeight();
    };

    /**
     * @method getSubtitleMetaText
     * @return {String}
     */
    this.getSubtitleMetaText = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return _.subtitleMetaText;
    };

    /**
     * @method setSubtitleMetaText
     * @param {String} subtitleMetaText
     */
    this.setSubtitleMetaText = subtitleMetaText => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(!this.getSubtitleText()) throw new Error('Expected subtitle text set');
        if(this.isSubtitleError()) throw new Error('Expected subtitle not error');
        if(typeof subtitleMetaText !== 'string') throw new TypeError('subtitleMetaText: Expected string');
        if(subtitleMetaText.length === 0) throw new TypeError('subtitleMetaText: Expected non-empty string');
        if(subtitleMetaText === this.getSubtitleMetaText()) throw new Error('subtitleMetaText: Expected different from current');
        $.group.add($.subtitleMetaText);
        $.subtitleMetaText.text(subtitleMetaText.toUpperCase());
        _.subtitleMetaText = subtitleMetaText;
    };

    /**
     * @method unsetSubtitleMetaText
     */
    this.unsetSubtitleMetaText = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(!this.getSubtitleMetaText()) throw new Error('Expected subtitle meta text set');
        $.subtitleMetaText.remove();
        _.subtitleMetaText = undefined;
    };

    /**
     * @method isSubtitleLoading
     * @return {Boolean}
     */
    this.isSubtitleLoading = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return $.subtitleLoading.isRunning();
    };

    /**
     * @method
     * @param {Boolean} isSubtitleLoading
     */
    this.setSubtitleLoading = isSubtitleLoading => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(typeof isSubtitleLoading !== 'boolean') throw new TypeError('isSubtitleLoading: Expected boolean');
        if(isSubtitleLoading){
            if(this.isSubtitleLoading()) throw new Error('Expected subtitle not loading');
            $.subtitleLoading.start();
        }
        else {
            if(!this.isSubtitleLoading()) throw new Error('Expected subtitle loading');
            $.subtitleLoading.stop();
            $.subtitleText.text(_.subtitleText.toUpperCase());
        }
    }

    /**
     * @method isSubtitleError
     * @return {Boolean}
     */
    this.isSubtitleError = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return $.subtitleBackground.fill() === colors.red && $.subtitleText.fill() === colors.white;
    };

    /**
     * @method setSubtitleError
     * @param {Boolean} isSubtitleError
     */
    this.setSubtitleError = isSubtitleError => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(!this.getSubtitleText()) throw new Error('Expected subtitle text set');
        if(this.getSubtitleMetaText()) throw new Error('Expected subtitle meta text not set');
        if(typeof isSubtitleError !== 'boolean') throw new TypeError('isSubtitleError: Expected boolean');
        if(isSubtitleError === this.isSubtitleError()) throw new Error('isSubtitleError: Expected different from current');
        if(isSubtitleError){
            $.subtitleBackground.fill(colors.red);
            $.subtitleText.fill(colors.white);
        }
        else {
            $.subtitleBackground.fill(colors.white);
            $.subtitleText.fill(colors.black);
        }
    };

    /**
     * @method getProgress
     * @returns {Number}
     */
    this.getProgress = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        return _.progress;
    };

    /**
     * @method setProgress
     * @param {Number} progress
     */
    this.setProgress = progress => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(this.getSubtitleText()) throw new Error('Expected subtitle text not set');
        if(typeof progress !== 'number') throw new TypeError('progress: Expected number');
        if(progress < 0) throw new RangeError('progress: Expected positive number');
        if(progress > 100) throw new RangeError('progress: Expected number lower or equal 100');
        if(!this.getProgress()){
            $.group.add($.subtitleBackground);
            $.group.add($.progressBar);
            $.group.add($.progressText);
        }
        const width = (this.getWidth() - BORDER_WIDTH) * progress / 100;
        $.progressBar.width(width);
        $.progressText.width(width);
        $.progressText.text(
            [`${progress.toFixed(2)} % `, `${progress.toFixed(1)} % `, `${Math.floor(progress)} % `]
                .find(text => width - $.progressText.measureSize(text).width >= 3)
        );
        _.progress = progress;
        _updateHeight();
    };

    /**
     * @method unsetProgress
     */
    this.unsetProgress = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(!this.getProgress()) throw new Error('Expected progress set');
        $.subtitleBackground.remove();
        $.progressBar.remove();
        $.progressText.remove();
        _.progress = undefined;
        _updateHeight();
    };

    /**
     * @method destroy
     */
    this.destroy = () => {
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        if(this.isTitleLoading())
            this.setTitleLoading(false);
        if(this.isTitleWarning())
            this.setTitleWarning(false);
        Window.windows.splice(Window.windows.indexOf(this), 1);
        $.group.destroy();
    };

    this.setPositionX(positionX);
    this.setPositionY(positionY);
    this.setWidth(width);
    this.setTitleText(titleText);
    this.setTitleColor(titleColor);
    if(isTitleLoading)
        this.setTitleLoading(isTitleLoading);
    if(isTitleWarning)
        this.setTitleWarning(isTitleWarning);
    if(subtitleText)
        this.setSubtitleText(subtitleText);
    if(subtitleMetaText)
        this.setSubtitleMetaText(subtitleMetaText);
    if(isSubtitleLoading)
        this.setSubtitleLoading(isSubtitleLoading);
    if(progress)
        this.setProgress(progress);

    $.group.add($.rectangle);

    Window.windows.push(this);

    _isInit = false;
}

/**
 * @property windows
 * @type {Window[]}
 */
Window.windows = [];

/**
 * @method setLayer
 * @param {Konva.Layer} layer
 */
Window.setLayer = layer => _layer = layer;

export default Window;