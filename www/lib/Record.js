const
    VIDEO_MIME_TYPE = 'video/webm',
    IMAGE_MIME_TYPE = 'image/webp';

/** @type {HTMLCanvasElement} */
let _canvas;

/**
 * @class Record
 * @param {String} [directory]
 * @param {String} name
 */
function Record({
    directory,
    name
}){
    if(!Record.isSupported()) throw new Error('Record feature not supported');

    if(typeof directory !== 'undefined'){
        if(typeof directory !== 'string') throw new TypeError('directory: Expected string');
        if(directory.length === 0) throw new TypeError('directory: Expected non-empty string');
        if(!/[\/\\]$/.test(directory)) throw new Error('directory: Expected trailing (anti)slash');
    }

    if(typeof name !== 'string') throw new TypeError('name: Expected string');
    if(name.length === 0) throw new TypeError('name: Expected non-empty string');
    if(!/^[\w-]+$/.test(name)) throw new Error('name: Expected valid file name');

    const
        record = new MediaRecorder(_canvas.captureStream(), { mimeType: `${VIDEO_MIME_TYPE}; codecs=vp9` }),
        res = [];

    let
        _isStarted = false,
        _isStopped = false;

    /**
     * @method start
     */
    this.start = () => {
        if(_isStarted) throw new Error('Expected record not started');
        if(_isStopped) throw new Error('Expected record not stopped');
        _isStarted = true;
        record.ondataavailable = ({ data }) => res.push(data);
        record.start();
    };

    /**
     * @method stopAndSave
     * @returns {Promise<String>}
     */
    this.stopAndSave = () => new Promise(resolve => {
        if(!_isStarted) throw new Error('Expected record started');
        if(_isStopped) throw new Error('Expected record not stopped');
        _isStopped = true;
        record.onstop = async () => {
            const path = `${directory || ''}${name}.webm`;
            // noinspection JSUnresolvedVariable, JSUnresolvedFunction
            await __TAURI__.fs.writeBinaryFile({
                contents: await new Blob(res, { type: VIDEO_MIME_TYPE }).arrayBuffer(),
                path
            }, directory ? {} : {
                dir: __TAURI__.fs.Dir.Video
            });
            resolve(path);
        };
        record.stop();
    });

    Record.records.push(this);
}

/**
 *
 * @type {Record[]}
 */
Record.records = [];

/**
 * @method setCanvas
 * @param {HTMLCanvasElement} canvas
 */
Record.setCanvas = canvas => _canvas = canvas;

Record.isSupported = () => document.createElement('canvas').toDataURL(IMAGE_MIME_TYPE).startsWith(`data:${IMAGE_MIME_TYPE}`);

export default Record;