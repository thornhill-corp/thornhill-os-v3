import Window from '../components/Window.js';
import Record from './Record.js';

const App = async () => {
    // noinspection JSUnresolvedVariable, JSUnresolvedFunction
    if(!await __TAURI__.window.appWindow.isFullscreen())
        // noinspection JSUnresolvedVariable, JSUnresolvedFunction
        await __TAURI__.window.appWindow.setFullscreen(true);

    const
        stage = new Konva.Stage({
            container: 'app',
            width: document.body.offsetWidth,
            height: document.body.offsetHeight
        }),
        layer = new Konva.Layer();

    stage.add(layer);

    const background = new Konva.Rect({
        fill: '#2A3D52',
        width: stage.width(),
        height: stage.height()
    });

    layer.add(background);

    window.addEventListener('resize', () => {
        stage.width(document.body.offsetWidth);
        stage.height(document.body.offsetHeight);
        background.width(stage.width());
        background.height(stage.height());
    });

    (function draw(){
        stage.draw();
        requestAnimationFrame(draw);
    })();

    Window.setLayer(layer);
    Record.setCanvas(layer.getCanvas()._canvas);

    return {
        Window,
        Record
    };
};

export default App;