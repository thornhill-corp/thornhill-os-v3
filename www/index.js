import App from './lib/App.js';

(async () => {
    const
        app = await App(),
        {
            Window
        } = app;

    Object.assign(window, app);

    {
        const loadingWindow = new Window({
            positionX: 50,
            positionY: 50,
            width: 300,
            titleText: 'Tracking admin',
            titleColor: 'yellow',
            isTitleLoading: true,
            isTitleWarning: false,
            subtitleText: 'Finch, Harold',
            subtitleMetaText: 'XXX-XX-5492',
            isSubtitleLoading: false,
            isSubtitleError: false,
            progress: undefined
        });
        loadingWindow.show();
    }
})();